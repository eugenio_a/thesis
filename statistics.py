import json
import constants
import helpers
import sys
from operator import itemgetter

ks = [1,50,500] 
precs = ['0.05', '0.15', '0.3', '0.5']

def borassiCompare(networkName=None, algo=None):
	with open(constants.borassi_path, 'r') as jfile:
		borassi_result = json.load(jfile)

	if algo == 'epps':
		with open(constants.epps_path, 'r') as jfile:
			epps_result = json.load(jfile)

		for net, epps_val in epps_result.items():
			print("Network = %s" %(net))
			for prec, epps_data in sorted(epps_result[net].items(), key=itemgetter(0)):
				if not networkName == None and net == (str(networkName) + ".csv"):
					for k, borassi_time in sorted(borassi_result[net].items(), key=itemgetter(0)):
						if int(k) in ks and prec in precs:
							epps_time = epps_data[constants.time_key]
							ratio = helpers.getRatio(borassi_time, epps_time)
							print('& %s & %s & %.2f\\%% \\\\' %(prec, k, ratio*100))
	elif algo == 'oka':
		with open(constants.oka_path + '05', 'r') as jfile:
			oka_result = json.load(jfile)
		with open(constants.ground_truth_filename, 'r') as jfile:
			gt_data = json.load(jfile)

		for net, oka_data in oka_result.items():
			print('Network = %s' %(net))
			for k, current_oka_data in sorted(oka_data.items(), key=itemgetter(0)):
				if net in borassi_result and str(k) in borassi_result[net]:
					borassi_time = (borassi_result[net])[str(k)]
					oka_time = helpers.revRatio(current_oka_data[constants.time_key], gt_data[net][constants.time_key])
					oka_prec = current_oka_data[constants.precision_key]
					ratio = helpers.getRatio(borassi_time, oka_time)
					print('& %s & %.2f\\%% \\\\' %(k, ratio*100))
	else:
		print('Algorithm for comparison not specified')

def exactCompare(algo):
	with open(constants.ground_truth_filename, 'r') as jfile:
		exact_data = json.load(jfile)

	if algo == 'epps':

		with open(constants.epps_path, 'r') as jfile:
			epps_data = json.load(jfile)

		#for net, epps_val in epps_data.items():
		for net in ['edit-enwikiquote.csv', 'edit-dewiktionary.csv']:
			print('Network = %s' %(net))
			for prec, apx_val in sorted(epps_data[net].items(), key=itemgetter(0)):
				exact_time = (exact_data[net])[constants.time_key]
				apx_time = apx_val[constants.time_key]
				samples = apx_val[constants.samples_key]
				ratio = helpers.getRatio(exact_time, apx_time)

				apx_values = apx_val[constants.values_key]
				exact_values = (exact_data[net])[constants.values_key]

				total_error = helpers.totalError(exact_values, apx_values)
				avg_error = total_error / len(apx_values)
				d_bound = avg_error / float(prec)
				var = helpers.variance(exact_values, apx_values)
				rel_error = helpers.relError(exact_values, apx_values)
				max_error = helpers.maxError(exact_values, apx_values)
				if len(prec) == 3:
					prec += '0'
				print('%s & %.2f & %s & %.2f\\%% & %.2f & %.2fe-3 & %.2fe-3 & %.2fe-6 & %.2fe-3 & %.2fe-3\\\\' %(
					prec, 
					float(apx_time), 
					samples, 
					float(ratio)*100, 
					total_error, 
					avg_error*1000, 
					d_bound*1000, 
					var*(10 ** 6),
					(rel_error)*1000,
					max_error*1000))
	
	elif algo == 'oka':
		with open(constants.oka_path+'', 'r') as jfile:
			oka_data = json.load(jfile)

		with open(constants.oka_path_first, 'r') as jfile:
			oka_data_first = json.load(jfile)

		for net, oka_val in oka_data.items():
			
			print('Network = %s' %(net))
			for k in [1,10,100]:#, oka_res in sorted(oka_val.items(), key=itemgetter(0)):
				exact_time = (exact_data[net])[constants.time_key]
				current_oka_data = oka_val[str(k)]
				oka_time = current_oka_data[constants.time_key]
				oka_prec = float(current_oka_data[constants.precision_key])
				samples = current_oka_data[constants.samples_key]
				add_samples = current_oka_data[constants.add_samples_key]
				ratio = oka_time * 100#(1 - float(helpers.getRatio(exact_time, oka_time))) * 100
				#oka_first_time = ((oka_data_first[net])[str(k)])[constants.time_key]
				# ratio2 = float(helpers.getRatio(oka_first_time, oka_time)) *100
				# print(oka_first_time)
				# print(oka_time)
				#print('%s & %.2f & %.2f\\%% \\\\' %(k, (1 - float(ratio))*100, float(oka_prec)))
				#print(oka_time - oka_first_time)
				if k == 1:
					print(samples, end='')
				print(' & %i & %.3f\\%% ' %( add_samples, ratio), end='')
				print('& %.2f\\%% \\\\' %(oka_prec))


def union(exact, apx):
	return len(set(apx).intersection(set(exact))) == len(exact)

def missesNumber(exact, apx):
	return len(exact) - len(set(apx).intersection(set(exact)))

def topkAnalysis(exact, apx, k):
	exact_topk = [x[0] for x in sorted(exact.items(), key=itemgetter(1))][0:k]
	apx_topk = [x[0] for x in sorted(apx.items(), key=itemgetter(1))]
	
	if union(exact_topk, apx_topk[0:k]):
		return [0,0]

	adds = 1
	n_miss = missesNumber(exact_topk, apx_topk[0:k])
	while k + adds < len(exact):
		if union(exact_topk, apx_topk[0:k+adds]):
			return [adds, n_miss]
		adds += 1
	return [k + adds, n_miss]


def topkCompare(networkName):
	with open(constants.ground_truth_filename, 'r') as jfile:
		exact_data = json.load(jfile)

	with open(constants.epps_path, 'r') as jfile:
		epps_data = json.load(jfile)

	if not networkName.endswith('.csv'):
		networkName += '.csv'

	if networkName in epps_data:
		print('Network found')
		for k in [1, 5, 10, 20, 50, 100]:
			print('k = %s' %(k))
			for prec, apx_val in sorted(epps_data[networkName].items(), key=itemgetter(0)):
				apx_values = apx_val[constants.values_key]
				exact_values = (exact_data[networkName])[constants.values_key]

				apx_values = {i: apx_values[i] for i in range(0,len(apx_values))}
				exact_values = {i: exact_values[i] for i in range(0, len(exact_values))}

				topk_a = topkAnalysis(exact_values, apx_values, k)
				adds = topk_a[0]
				misses = topk_a[1]
				if (len(prec) == 3):
					prec += '0'

				print("%s & %i & %i & %.2f\\\\" %(prec, misses, adds, (k + adds) / k))

	else:
		print('No netowork found')

def main():
	
	args = sys.argv

	if len(args) > 2:
		opt = args[2]
		if args[1] == 'epps':
			if opt == 'borassi':
				if len(args) > 3:
					net = args[3]
					borassiCompare(net, args[1])
			elif opt == 'exact':
				exactCompare(args[1])
			elif opt == 'topk':
				net = ''
				if len(args) > 3:
					net = args[3]
					topkCompare(net)
		elif args[1] == 'oka':
			if opt == 'borassi':
				if len(args) > 3:	
					borassiCompare(args[3], args[1])
				else:
					borassiCompare(algo=args[1])
			elif opt == 'exact':
				exactCompare(args[1])

if __name__ == '__main__':
	main()