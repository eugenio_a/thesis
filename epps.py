from graph_tool.all import *
import random
import constants
import sys
import math
import numpy as np

# Returns the optimal number of samples
# in order to get the required precision
def numberOfSamples(n, prec):
	if n <= 0 or prec <= 0 or prec > 1:
		return 1

	# Scientific round, return values
	# between 1 and n
	return min(n, max(1, math.ceil(0.5 + constants.samplesConstants * math.log(n) / pow(prec, 2))))

# Function to perform the Eppstein
# approach for harmonic centrality
def Eppstein(G, prec):

	# Number of nodes of graph G
	n = G.num_vertices()

	# In some cases (e.g. okamoto) we call
	# Eppstein(G, prec) function with a pre
	# calculated number of samples
	if (prec >= 1):
		# Precision interpreted as number of samples
		l = prec
	else:
		# Precision interpreted as itself, now
		# the optimal number of samples is 
		# calculated through the function
		l = numberOfSamples(n, prec)

	# List of unique random chosen vertices
	r_chosen = [G.vertex(v) for v in random.sample(range(0, n - 1), l)]

	# Performs SSSP from each node in v to each node in 
	# the set 'r_chosed' (in accordance with the paper).

	mult_fact = (n / (l * (n - 1)))
	max_dist = G.num_vertices() + 1

	if G.is_directed():
		approx_harmonics = [shortest_distance(G, source=v, target=r_chosen, max_dist=max_dist) for v in G.vertices()]
	else:
		approx_harmonics = [shortest_distance(G, source=G.vertex(v), max_dist=max_dist).get_array() for v in r_chosen]
		transposed_h = np.transpose(approx_harmonics)

	return [(1. / dists[(dists < max_dist) * (dists > 0)]).sum() * mult_fact for dists in transposed_h]

def main():
	# Default values
	inputGraph = './networks/3437.csv'
	prec = 0.4	# Percision to achieve per single nodes

	# Read command line arguments
	args = sys.argv
	if (len(args) > 1):
		prec = min(max(float(args[2]), 0), 1) # Check if the value is between 0 and 1

	# Load graph from file
	G = load_graph_from_csv(inputGraph, directed=False, csv_options={'delimiter': ' '})

	# Computation of the number of k samples
	# and check that k is not greater than n
	k = numberOfSamples(G.num_vertices(), prec)
	Eppstein(G, k)

if __name__ == '__main__':
	main()