import networkx as nx
from epps import Eppstein as epps
import sys
import math
from operator import itemgetter
from exact_harmonic import harmonic
from bisect import bisect_left
import constants

# Function f(l)
def f(alpha, n, l):
	if l == 0 or n < 1:
		return 0	
	return alpha * math.sqrt(math.log(n)) / l

# Function to calculate the most appropriate value for 'l'
def lcalc(n):
	if n <= 0:
		return None

	# Asymptotic value for 'l' as reported in
	# the paper
	l = constants.oka_samples_const * pow(n, 2 / 3) * pow(math.log(n), 1 / 3)

	# Scientific rounding of 'l' because
	# 'l' must be an integer
	return math.ceil(l + 0.5)

def Okamoto(G, k):
	# 1 if we totally trust in Eppstein
	# high if not
	alpha = constants.oka_const

	# Number of nodes in G
	n = G.num_vertices()

	# Samples for epps.py
	l = lcalc(n)

	# Estimated harmonic centrality calculated with Eppstein and 'l' samples
	# Order must not be reversed in order to use bisect_left correctly
	epps_dict = epps(G, l)
	epps_dict = {str(i): epps_dict[i] for i in range(0, n)}
	hs = sorted(epps_dict.items(), key=itemgetter(1))

	# Calculating h_k - 2f(l):
	threshold = hs[n - k - 1][1] - 2 * f(alpha, n, l)

	# Index of the threshold in the 'harmonics' list
	E_index = bisect_left([x[1] for x in hs], threshold)

	# Check if there is at least one centrality to select
	if E_index > n - 1:
		return {}

	# Calculating exact topk harmonic centralities
	# for each node in set E
	topK = sorted((harmonic(G, [x[0] for x in hs[E_index: n]])).items(), key=itemgetter(1), reverse=True)

	# Including also other nodes with harmonic
	# centrality equals to h_k
	to_k = k - 1
	while to_k < len(topK) - 1 and topK[to_k + 1] == topK[to_k]:
		to_k += 1

	# Top k harmonic centralities
	return [topK[0: to_k + 1],  n - E_index - 1]

def main():
	# Default values
	inputGraph = './networks/1684.csv'
	k = 20 # Default value for top K

	# Read command line arguments
	args = sys.argv
	if (len(args) > 1):
		k = min(k, int(args[2]))
		if (len(args) > 2):
			inputGraph = str(args[1])

	# Load graph from file
	G = load_graph_from_csv(inputGraph, directed=False, csv_options={'delimiter': ' '})
	Okamoto(G, k)

if __name__ == '__main__':
	main()