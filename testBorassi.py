from networkit import *
import time
import os
import json
import helpers
import constants

# Checks whether the result file is present or not.
# If not it creates a new blank result file
def checkResultFile():
	try:
		with open(constants.borassi_path, 'r') as f:
			pass
	except FileNotFoundError:
		with open(constants.borassi_path, 'w') as f:
			json.dump(dict(), f)

# Saves current result in a dictionary
def writeResult(f, time_elapsed, k):
	checkResultFile()

	with open(constants.borassi_path, 'r') as jfile:
		data = json.load(jfile)

	if f in data:
		current_file_data = data[f]
		current_file_data.update(dict.fromkeys([k], time_elapsed))
		data.update(dict.fromkeys([f], current_file_data))
	else:
		newDict = dict.fromkeys([k], time_elapsed)
		data.update(dict.fromkeys([f], newDict))

	with open(constants.borassi_path, 'w') as jfile:
		json.dump(data, jfile)

def main():

	# Read command line arguments
	args = sys.argv

	# Number of times the algorithm will be
	# executed in order to get a better
	# time performance estimation
	iterations = 1
	if (len(args) > 1):
		iterations = max(iterations, int(args[1]))

	result_to_write = {}

	# Loop for each file in the
	# directory '/networks'
	for f in os.listdir(constants.root):

		if helpers.fileFormat(f):

			# Path to current network file
			path = constants.root + '/' + f

			# Reads and creates a new graph object
			G = readGraph(path , Format.EdgeListSpaceOne)

			for k in constants.k_list:

				print('Running Borassi et al. algorithm for graph %s, k = %i ...' %(f, k))

				total_time = 0

				for i in range(0, iterations):

					print('Iteration #%i' %(i + 1))

					# Timestamp before the algorithm starts
					start = time.perf_counter()

					# Executing the algorithm
					centrality.TopCloseness(G, sec_heu=False, k=k).run()

					time_elapsed = time.perf_counter() - start
					total_time += time_elapsed

					print('Finished in %s seconds' %(time_elapsed))

				avg_time = total_time / iterations

				print('Writing result to disk...')
				writeResult(f, avg_time, k)


if __name__ == '__main__':
	main()