import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from pylab import *
from matplotlib.ticker import OldScalarFormatter, ScalarFormatter
import constants
from graph_tool.all import *
import json
from operator import itemgetter
import numpy as np
import helpers
import os
import sys
from statistics import topkAnalysis

figure = 1
data = {}
maxError = 0
maxValue = 0
columns = 100

font = {'family' : 'serif',
        'weight' : 'medium',
        'size'   : 22}

def plotErrorHist(errors, networkName, precision):
	global maxError
	global maxValue
	global data
	global columns
	data[str(precision)] = errors
	maxError = max(maxError, max(np.absolute(errors)))
	y, x, _ = plt.hist(errors, columns, normed=1)

	maxValue = max(maxValue, y.max())
	plt.clf()

def drawHist():
	i = 0
	global figure
	global maxError
	global columns
	global maxValue
	prec = helpers.getNextPrec(i)
	while prec <= constants.prec_to:

		plt.figure(figure)
		figure += 1


		plt.xlabel('Error')
		plt.ylabel('Affected vertices')
		plt.axis((-maxError, maxError, 0, maxValue))
		plt.hist(data[str(prec)], 
				columns,
				normed=1, 
				cumulative=0, 
				histtype='stepfilled',
				facecolor='blue',
				alpha=1)
		i += 1
		prec = helpers.getNextPrec(i)
	showPlots()

def plotTopKError(error):
	i = 0
	p = helpers.getNextPrec(i)
	while p <= prec_to:

		for f in os.listdir(constants.root):
			
			for current_errors in error[f]:

				current_error_list = current_errors[i]
				p = helpers.getNextPrec(i)
				averages = []
				d_bounds = []
				variances = []
				for k in range(1, 100):
					cur_err = error[0:k]
					max_error = max(cur_err)
					averages.append(sum(cur_err / k))
					d_bounds.append(avg / p)
					#variances.append(sum((cur_err .- avg).^2) / max(1, k - 1))

		i += 1
		p = helpers.getNextPrec(i)

time_precs = [0.05, 0.1]
social_nets = ['Advogato.csv', 'Anybeat.csv', 'Brightkite.csv', 'Gowalla.csv', 'Epinions trust.csv', 'Gplus.csv', 'Wiki-elec.csv', 'facebook_combined.csv', 'Epinions.csv']
def plotEppsTime():
	with open(constants.ground_truth_filename, 'r') as f:
		exact_data = json.load(f)

	with open(constants.epps_path, 'r') as f:
		epps_data = json.load(f)

	vertices_social = []
	vartices_aut = []
	line1_social = []
	line2_social = []
	line1_aut = []
	line2_aut = []
	for net, epps_val in epps_data.items():
		g = helpers.csvToGraph('./networks/' + net)
		exact_time = (exact_data[net])[constants.time_key]
		apx_time1 = ((epps_data[net])[str(time_precs[0])])[constants.time_key]
		apx_time2 = ((epps_data[net])[str(time_precs[1])])[constants.time_key]

		if net in social_nets:
			vertices_social.append(g.num_vertices())
			line1_social.append(helpers.getRatio(exact_time, apx_time1))
			line2_social.append(helpers.getRatio(exact_time, apx_time2))
		else:
			vartices_aut.append(g.num_vertices())
			line1_aut.append(helpers.getRatio(exact_time, apx_time1))
			line2_aut.append(helpers.getRatio(exact_time, apx_time2))

	plt.rc('font', **font)

	fig1 = plt.figure(1)
	ax1 = fig1.add_subplot(111)
	plt.xlabel('Number of vertices', fontsize=22)
	plt.ylabel('Gain', fontsize=22)
	data = zip(vertices_social, zip(line1_social, line2_social))
	data = sorted(data, key=itemgetter(0))
	line1_social = [x[1][0] for x in data]
	line2_social = [x[1][1] for x in data]
	data = [x[0] for x in data]

	plotLine1, = plt.plot(data, line1_social, 'r', label='$\epsilon = 0.05$', linewidth=3)
	plotLine2, = plt.plot(data, line2_social, 'b', label='$\epsilon = 0.1$', linewidth=3)

	plt.legend(handles=[plotLine1, plotLine2], loc=4)
	plt.plot(data, line1_social, 'ro', data, line2_social, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

	fig2 = plt.figure(2)
	ax2 = fig2.add_subplot(111)
	plt.xlabel('Number of vertices', fontsize=22)
	plt.ylabel('Gain', fontsize=22)
	data = zip(vartices_aut, zip(line1_aut, line2_aut))
	data = sorted(data, key=itemgetter(0))
	line1_aut = [x[1][0] for x in data]
	line2_aut = [x[1][1] for x in data]
	data = [x[0] for x in data]

	plotLine3, = plt.plot(data, line1_aut, 'r', label='$\epsilon = 0.05$', linewidth=3)
	plotLine4, = plt.plot(data, line2_aut, 'b', label='$\epsilon = 0.1$', linewidth=3)

	plt.legend(handles=[plotLine3, plotLine4], loc=4)
	plt.plot(data, line1_aut, 'ro', data, line2_aut, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

	plt.show()

networkNames = ['Wikiquote (En)', 'Wiktionary (De)']
xlabelString = 'Error upper bound'
def plotEpsilonRatio(networks):
	with open(constants.epps_path, 'r') as f:
		epps_data = json.load(f)

	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	i = 0
	precs = []
	line1 = []
	line2 = []
	prec = helpers.getNextPrec(i)   	
	while prec <= constants.prec_to:
		precs.append(prec)
		apx = (epps_data[networks[0]][str(prec)])[constants.values_key]
		exact_val = exact_data[networks[0]][constants.values_key]
		total_error1 = helpers.totalError(exact_val, apx)
		line1.append(1 / (total_error1 / (prec * len(apx))))

		exact_val = exact_data[networks[1]][constants.values_key]
		apx2 = (epps_data[networks[1]][str(prec)])[constants.values_key]
		total_error2 = helpers.totalError(exact_val, apx2)
		line2.append(1 / (total_error2 / (prec * len(apx2))))
		i += 1
		prec = helpers.getNextPrec(i)


	fig1 = plt.figure(1)
	ax1 = fig1.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('$\epsilon$ / avg. error', fontsize=22)

	timeLine1, = plt.plot(precs, line1, 'r', label=networkNames[0], linewidth=3)
	timeLine2, = plt.plot(precs, line2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[timeLine1, timeLine2], loc=2)
	plt.plot(precs, line1, 'ro', precs, line2, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

	plt.show()

	

def plotPrecVsTime(networks):
	with open(constants.epps_path, 'r') as f:
		epps_data = json.load(f)

	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	time1 = []
	time2 = []
	avg_error1 = []
	avg_error2 = []
	dbound1 = []
	dbound2 = []
	var1 = []
	var2 = []
	rel1 = []
	rel2 = []
	max1 = []
	max2 = []
	precs = []
	i = 0
	prec = helpers.getNextPrec(i)
	while prec <= constants.prec_to:
		precs.append(prec)
		epps_val = epps_data[networks[0]]
		exact_val = exact_data[networks[0]]
		n= len(exact_val[constants.values_key])
		
		avg1 = helpers.totalError(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key])/ n

		time1.append(helpers.getRatio(exact_val[constants.time_key], (epps_val[str(prec)])[constants.time_key]))
		avg_error1.append(avg1)
		dbound1.append(avg1 / prec)
		var1.append(helpers.variance(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key]))
		rel1.append(helpers.relError(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key]))
		max1.append(helpers.maxError(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key]))

		epps_val = epps_data[networks[1]]
		exact_val = exact_data[networks[1]]
		
		avg2 = helpers.totalError(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key])/ n

		time2.append(helpers.getRatio(exact_val[constants.time_key], (epps_val[str(prec)])[constants.time_key]))
		avg_error2.append(avg2)
		dbound2.append(avg2 / prec)
		var2.append(helpers.variance(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key]))
		rel2.append(helpers.relError(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key]))
		max2.append(helpers.maxError(exact_val[constants.values_key], (epps_val[str(prec)])[constants.values_key]))

		i += 1
		prec = helpers.getNextPrec(i)

	fig1 = plt.figure(1)
	ax1 = fig1.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('Time gain', fontsize=22)

	timeLine1, = plt.plot(precs, time1, 'r', label=networkNames[0], linewidth=3)
	timeLine2, = plt.plot(precs, time2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[timeLine1, timeLine2], loc=4)
	plt.plot(precs, time1, 'ro', precs, time2, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


	fig2 = plt.figure(2)
	ax2 = fig2.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('Average absolute error', fontsize=22)

	plotLine3, = plt.plot(precs, avg_error1, 'r', label=networkNames[0], linewidth=3)
	plotLine4, = plt.plot(precs, avg_error2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[plotLine3, plotLine4], loc=2)
	plt.plot(precs, avg_error1, 'ro', precs, avg_error2, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


	fig3 = plt.figure(3)
	ax3 = fig3.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('d_bound', fontsize=22)

	plotLine5, = plt.plot(precs, dbound1, 'r', label=networkNames[0], linewidth=3)
	plotLine6, = plt.plot(precs, dbound2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[plotLine5, plotLine6], loc=1)
	plt.plot(precs, dbound1, 'ro', precs, dbound2, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


	fig4 = plt.figure(4)
	ax4 = fig4.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('Error variance', fontsize=22)

	plotLine7, = plt.plot(precs, var1, 'r', label=networkNames[0], linewidth=3)
	plotLine8, = plt.plot(precs, var2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[plotLine7, plotLine8], loc=2)
	plt.plot(precs, var1, 'ro', precs, var2, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


	fig5 = plt.figure(5)
	ax5 = fig5.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('Average relative error', fontsize=22)

	plotLine9, = plt.plot(precs, rel1, 'r', label=networkNames[0], linewidth=3)
	plotLine10, = plt.plot(precs, rel2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[plotLine9, plotLine10], loc=2)
	plt.plot(precs, rel1, 'ro', precs, rel2, 'bo', linewidth=3)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


	fig6 = plt.figure(6)
	ax6 = fig6.add_subplot(111)
	plt.xlabel(xlabelString, fontsize=22)
	plt.ylabel('Maximum error', fontsize=22)

	plotLine11, = plt.plot(precs, max1, 'r', label=networkNames[0], linewidth=3)
	plotLine12, = plt.plot(precs, max2, 'b', label=networkNames[1], linewidth=3)

	plt.legend(handles=[plotLine11, plotLine12], loc=2)
	plt.plot(precs, max1, 'ro', precs, max2, 'bo', linewidth=2)
	plt.grid(True)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

	plt.show()

topk_precs = [0.05, 0.1, 0.25, 0.5]
def plotTopkPrec(networks):
	with open(constants.epps_path, 'r') as f:
		epps_data = json.load(f)

	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	figure_number = 1
	ks = [1] + [x*5 for x in range(1,101)] 

	for net in networks:
		epps_val = epps_data[net]
		exact_val = exact_data[net]
		exact_values = exact_val[constants.values_key]
		lines = [[0]*len(ks) for x in topk_precs]
		k_num = 0
		for k in ks:
			for x in range(0,len(topk_precs)):
				epps_values = (epps_val[str(round(topk_precs[x], 2))])[constants.values_key]

				epps_dict = {i: epps_values[i] for i in range(0,len(epps_values))}
				exact_dict = {i: exact_values[i] for i in range(0,len(exact_values))}
				lines[x][k_num] = (k + topkAnalysis(exact_dict, epps_dict, k)[0]) / k
			k_num += 1

		fig = plt.figure(figure_number)
		ax = fig.add_subplot(111)
		plt.xlabel('k', fontsize=22)
		plt.ylabel('Ratio', fontsize=22)

		plotLines = []
		for x in range(0,len(topk_precs)):
			plotLine, = plt.plot(ks, lines[x], label='$\epsilon = %.2f$' %(topk_precs[x]), linewidth=2)
			plotLines.append(plotLine)

		plt.legend(handles=plotLines, loc=1)
		plt.grid(True)
		plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
		figure_number += 1
	plt.show()

def plotTopkRel():
	with open(constants.epps_path, 'r') as f:
		epps_data = json.load(f)

	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	current_k = 0
	figure_number = 0
	precs = [0.05, 0.1, 0.25, 0.5]
	for net in ['edit-dewiktionary.csv' ,'edit-enwikinews.csv']:
		top = int(len(epps_data[net][str(precs[0])][constants.values_key]))
		x_data = range(0, top)
		lines = []

		for p in precs:
			line = []
			apx = epps_data[net][str(p)][constants.values_key]
			exact_val = exact_data[net][constants.values_key]

			apx = {i: apx[i] for i in range(0, len(apx))}
			exact = {i: exact_val[i] for i in range(0, len(apx))}
			exact = sorted(exact.items(), key=itemgetter(1), reverse=True)
			apx = [apx[i] for i in [x[0] for x in exact]]
			exact = [x[1] for x in exact]

			relsum = 0
			samp = 1
			for i in range(0, top):
				if not exact[i] == 0:
					relsum += np.absolute(apx[i] - exact[i]) / exact[i]
					line.append(relsum / samp)
				else:
					relsum += apx[i]
					line.append(apx[i] / samp)
				samp += 1

			lines.append(line)

		plt.rc('font', **font)

		fig = plt.figure(figure_number)
		ax1 = fig.add_subplot(111)
		plt.xlabel('Top centralities')
		plt.ylabel('Average relative error')
		plotLines = []
		p = 0
		for l in lines:
			plotLine, = plt.plot(x_data, l, label = '$\epsilon = %.2f$' %(precs[p]), linewidth=2)
			plotLines.append(plotLine)
			p += 1 

		plt.legend(handles=plotLines, loc=1)
		plt.grid(True)
		plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
		plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

		figure_number += 1
	plt.show()

def plotTopkErr():
	with open(constants.epps_path, 'r') as f:
		epps_data = json.load(f)

	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	current_k = 0
	net = 'edit-enwikinews.csv'
	figure_number = 0
	precs = [0.05, 0.1, 0.25, 0.5]
	for net in ['edit-dewiktionary.csv' ,'edit-enwikinews.csv']:
		lines = []
		top = int(len(epps_data[net][str(precs[0])][constants.values_key]) / 2)
		x_data = range(0, top)

		for p in precs:
			line = []
			apx = epps_data[net][str(p)][constants.values_key]
			exact_val = exact_data[net][constants.values_key]

			apx = {i: apx[i] for i in range(0, len(apx))}
			exact = {i: exact_val[i] for i in range(0, len(apx))}
			exact = sorted(exact.items(), key=itemgetter(1), reverse=True)
			apx = [apx[i] for i in [x[0] for x in exact]]
			exact = [x[1] for x in exact]

			err_sum = 0
			samp = 1
			for i in range(0,top):
				err_sum += np.absolute(apx[i] - exact[i])
				line.append(err_sum / samp)
				samp += 1

			lines.append(line)

		plt.rc('font', **font)

		fig = plt.figure(figure_number)
		ax1 = fig.add_subplot(111)
		plt.xlabel('Top centralities')
		plt.ylabel('Average error')
		plotLines = []
		p = 0
		for l in lines:
			plotLine, = plt.plot(x_data, l, label = '$\epsilon = %.2f$' %(precs[p]), linewidth=2)
			plotLines.append(plotLine)
			p += 1 

		plt.legend(handles=plotLines, loc=1)
		plt.grid(True)
		plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
		plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

		figure_number += 1
	plt.show()

def plotTopkCentralities():
	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	top = 251
	x_data = range(0,top)
	lines = []
	for net in ['edit-dewiktionary.csv', 'edit-enwikinews.csv']:
		values = exact_data[net][constants.values_key]
		values = sorted(values, reverse=True)[0:top]
		lines.append(values)

	plt.rc('font', **font)
	fig = plt.figure(1)
	ax1 = fig.add_subplot(111)
	plt.xlabel('Top centralities')
	plt.ylabel('Centrality value')
	plotLines = []
	names = ['Wiktionary (de)', 'Wikinews (en)']
	n = 0
	for l in lines:
		plotLine, = plt.plot(x_data, l, label = names[n], linewidth=3)
		plotLines.append(plotLine)
		n += 1
	plt.legend(handles=plotLines, loc=1)
	plt.grid(True)
	x1,x2,y1,y2 = plt.axis()

	plt.axis((x1,x2,0,1))
	plt.show()


def format_decimal(number):
 	n = str(number)
 	tail = n[1]
 	if int(n[2]) >= 5:
 		tail = str(int(tail)+1)
 	return n[0] + "." + tail+"e"+str(len(n)-1)


def plotOka(opt=None):
	with open(constants.oka_path_first, 'r') as f:
		oka_data = json.load(f)

	with open(constants.ground_truth_filename, 'r') as f:
	    exact_data = json.load(f)

	ks = constants.k_list
	lines_soc = [[0]*len(social_nets) for x in ks]
	lines_aut = [[0]*9 for x in ks]
	vertices_social = []
	vertices_aut = []
	current_soc_net = 0
	current_aut_net = 0
	for net, oka_items in oka_data.items():
		g = helpers.csvToGraph('./networks/tmp/' + net)

		current_k = 0
		for k in ks:


			if net in social_nets:
				lines_soc[current_k][current_soc_net] = oka_data[net][str(k)][constants.time_key]

			else:
				lines_aut[current_k][current_aut_net] = oka_data[net][str(k)][constants.time_key]
			current_k += 1
		if net in social_nets:
			vertices_social.append(g.num_vertices())
			current_soc_net += 1
		else:
			vertices_aut.append(g.num_vertices())
			current_aut_net += 1

	plt.rc('font', **font)

	fig1 = plt.figure(1)
	ax1 = fig1.add_subplot(111)
	plt.xlabel('Number of vertexes', fontsize=22)
	plt.ylabel('Gain', fontsize=22)
	plotLines1 = []
	current_k = 0
	plt.ylim(ymin=0.3)
	for current_line in lines_soc:
		social_data = sorted(zip(vertices_social, current_line), key=itemgetter(0))
		x_axis = [x for x in range(0,len(social_data))]
		y_axis = [x[1] for x in social_data]
		plotLine, = plt.plot(x_axis, y_axis, label='K = %i' %(ks[current_k]), linewidth=2)
		plotLines1.append(plotLine)
		current_k += 1
	plt.legend(handles=plotLines1 ,loc=4)
	plt.title('Social networks')
	new_x = [format_decimal(x) for x in sorted(vertices_social)]
	plt.xticks(x_axis, new_x)
	#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
	plt.grid(True)

	fig2 = plt.figure(2)
	ax2 = fig2.add_subplot(111)
	plt.xlabel('Number of vertexes', fontsize=22)
	plt.ylabel('Gain', fontsize=22)
	plt.title('Authorship networks')
	plotLines2 = []
	
	current_k = 0

	plt.ylim(ymin=0.8)
	for current_line in lines_aut:
		social_data = sorted(zip(vertices_aut, current_line), key=itemgetter(0))
		x_axis = [x for x in range(0,len(social_data))]
		y_axis = [x[1] for x in social_data]
		plotLine, = plt.plot(x_axis, y_axis, label='K = %i' %(ks[current_k]), linewidth=2)
		plotLines2.append(plotLine)
		current_k += 1
	new_x = [format_decimal(x) for x in sorted(vertices_aut)]
	plt.xticks(x_axis, new_x)
	#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
	plt.legend(handles=plotLines2, loc=4)
	#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
	plt.grid(True)
	plt.show()


def main():
	args = sys.argv

	if len(args) > 1:
		if args[1] == "epps":
			if len(args) > 2:
				if args[2] == "time":
					plotEppsTime()
				elif args[2] == "epsratio":
					plotEpsilonRatio(['edit-enwikiquote.csv', 'edit-dewiktionary.csv'])
				elif args[2] == "prec":
					plotPrecVsTime(['edit-enwikiquote.csv', 'edit-dewiktionary.csv'])
				elif args[2] == 'topkrel':
					plotTopkRel()
				elif args[2] == 'topkerr':
					plotTopkErr()
				elif args[2] == "topk":
					plotTopkPrec(['edit-enwikinews.csv', 'edit-dewiktionary.csv'])
				elif args[2] == 'topkcent':
					plotTopkCentralities()

		elif args[1] == "oka":
			if len(args) > 2:
				plotOka(args[2])

if __name__ == '__main__':
	main()