from graph_tool.all import *
import os
import sys
import json
import constants
from enum import Enum
import time
from operator import itemgetter
from okamoto import Okamoto
from okamoto import lcalc
import helpers

# Performance metric to measure for Eppstein
class Options(Enum):
	single_k_mult_net = 0
	mult_k_single_net = 1


def checkResultFile():
	try:
		with open(constants.oka_path, 'r') as jfile:
			pass
	except FileNotFoundError:
			with open(constants.oka_path, 'w') as jfile:
				json.dump({}, jfile)

def writeResult(network, k, time_elapsed, values, prec, samples, add_samples):
	checkResultFile()

	with open(constants.oka_path, 'r') as jfile:
		data = json.load(jfile)

	current_k_dict = dict.fromkeys([constants.time_key], time_elapsed)
	current_k_dict.update(dict.fromkeys([constants.precision_key], prec))
	current_k_dict.update(dict.fromkeys([constants.samples_key], samples))
	current_k_dict.update(dict.fromkeys([constants.add_samples_key], add_samples))
	current_k_dict.update(dict.fromkeys([constants.values_key], values))
	current_k_dict = dict.fromkeys([k], current_k_dict)

	if network in data:
		dictToUpdate = data[network]
		dictToUpdate.update(current_k_dict)
		dictToUpdate = dict.fromkeys([network], dictToUpdate)
	else:
		dictToUpdate = dict.fromkeys([network], current_k_dict)

	data.update(dictToUpdate)

	with open(constants.oka_path, 'w') as jfile:
		json.dump(data, jfile)


def main():

	# Checks if ground truth file is present
	# (no reason to continue if not)
	helpers.checkGroundTruth()

	# Initialization
	k = 1
	iterations = 1
	opt = Options.single_k_mult_net

	# Read command line arguments
	args = sys.argv
	if (len(args) > 1):
		# This argument represents k.
		# Chcking if k >= 1
		k = max(1, int(args[1]))
	else:
		print('No k value provided!')
		sys.exit()

	if (len(args) > 2):
		# This argument represents
		# the kind of experiment: 0 for multiple
		# networks and a single k, 1 for a single
		# network and multiple k
		if int(args[2]) == 1:
			opt = Options.mult_k_single_net
			#print('\n{:>12} {:>12} {:>12} {:>12}\n\n'.format('k', 'TIME', 'IMPROVEMENT', 'PRECISION'))
		#else:
			#print('\n{:>12}  {:>12}  {:>12} {:>12} {:>12}\n\n'.format('NETWORK', 'SAMPLES', 'K', 'RATIO', 'PRECISION'))

	if (len(args) > 3):
		# This argument represents the number
		# of times the algorithm will be performed
		# in order to get a better performance estimation
		# by averaging the results
		iterations = int(args[3])
		 

	# Listing each file in ./networks 
	# directory
	for network in os.listdir(constants.root):
		if helpers.fileFormat(network):
			path = constants.root + '/' + network
			print('Loading %s ...' %(network))

			G = helpers.csvToGraph(path)
			print('Done!')
			print('Running Okamoto for graph %s ...' %(network))

			if opt == Options.mult_k_single_net:
				performMultiKTest(G, iterations, network)
			else:
				performTimeKTest(G, k, iterations, network)

# Performs the time test using Okamoto algorithm for a single given k
# and averages over the different iterations
def performTimeKTest(G, k, iterations, network):
	with open(constants.ground_truth_filename, 'r') as json_file:
	 	data = json.load(json_file)

	exact_result = data[str(network)]
	exact_data = exact_result[constants.values_key]
	exact_seconds = getExactAlgorithmTime(network)
	total_time = 0
	samples = lcalc(G.num_vertices())
	 #f(constants.oka_const, G.num_vertices(), samples)
	overall_precision = 0
	all_topk = []
	for i in range(0, iterations):
		start = time.perf_counter()
		result_data = Okamoto(G, k)
		time_elapsed = time.perf_counter() - start
		print('Finished in %s seconds' %(time_elapsed))
		total_time += time_elapsed
		current_topk = [float(x[1]) for x in result_data[0]]
		add_samples = result_data[1]
		overall_precision += helpers.checkTopK(exact_data, current_topk)[0]
		all_topk = list(set(all_topk).union(current_topk))

	total_time /= iterations
	overall_precision /= iterations
	ratio = helpers.getRatio(exact_seconds, total_time)
	
	writeResult(network, k, ratio, all_topk, overall_precision, samples, add_samples)
	#res_string = '{:>12}  {:>12}  {:>12}  {:>12}  {:>12}\n'.format(f, k, samples, ratio, str()+ "%")


# Performs the time test using Okamoto algorithm for a list of
# different k, averages over the different iterations
def performMultiKTest(G, iterations, network):

	with open(constants.ground_truth_filename, 'r') as jfile:
		data = json.load(jfile)

	exact_result = data[str(network)]
	exact_data = exact_result[constants.values_key]
	exact_seconds = getExactAlgorithmTime(network)
	samples = lcalc(G.num_vertices())

	for current_k in constants.k_list:
		#if helpers.checkNetK(network, current_k):
			print('K = %i' %(current_k))
			total_time = 0
			overall_precision = 0
			all_topk = []
			for i in range(0, iterations):
				start = time.perf_counter()
				result_data = Okamoto(G, current_k)
				time_elapsed = time.perf_counter() - start
				print('Finished in %s seconds' %(time_elapsed))
				total_time += time_elapsed
				current_topk = [float(x[1]) for x in result_data[0]]
				add_samples = result_data[1]
				overall_precision += helpers.checkTopK(exact_data, current_topk)[0]
				all_topk = list(set(all_topk).union(current_topk))

			total_time /= iterations
			overall_precision /= iterations
			ratio = helpers.getRatio(exact_seconds, total_time)

			writeResult(network, current_k, ratio, all_topk, overall_precision, samples, add_samples)
		#else:
		#	print('k = %i already done for network %s' %(current_k, network))
		#res_string = '\n{:>12} {:>12} {:>12} {:>12}\n'.format(current_k, total_time, ratio, '-')#str(helpers.checkTopK(exact_values, result_data)) + '%')
		#print(res_string)
		#printToFile(res_string)


# Prints the given string to the results file
def printToFile(s):
	result_file = open(constants.oka_results, 'a')
	result_file.write(s)
	result_file.close()

# Reads and return the time required by the basic algorithm
# to execute on the given file 'f'
def getExactAlgorithmTime(network):
	with open(constants.ground_truth_filename, 'r') as json_file:
	 	data = json.load(json_file)

	exact_result = data[str(network)]
	return exact_result[constants.time_key]

if __name__ == '__main__':
	main()