import json
import constants 
import os
import sys
import helpers
import numpy as np
from operator import itemgetter
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

figure = 1
ks = 100

def main():
	args = sys.argv
	if len(args) > 1:
		if (args[1]) == 'topk':
			plotTopKErrors()
	else:
		plotErrors()

def plotErrors():
	global figure

	with open(constants.epps_prec_results_json, 'r') as jfile:
		data = json.load(jfile)
	with open(constants.ground_truth_filename, 'r') as jfile:
		gt = json.load(jfile)

	x_axis = [helpers.getNextPrec(i) for i in range(0, int(constants.prec_to / constants.prec_gran) )]
	plotLines1 = []
	plotLines2 = []
	for f in os.listdir(constants.root):
		exact_data = gt[f]
		exact = exact_data[constants.values_key]

		if helpers.fileFormat(f):
			apx_data = data[f]
			print("NETWORK = %s" %(f))
			d_bounds = []
			avg_errors = []

			i = 0
			p = helpers.getNextPrec(i)
			while p <= constants.prec_to:
				apx = apx_data[str(p)]
				apx_val = apx[constants.values_key]
				error = sortedErrors(apx_val, exact)
				variance = np.subtract(apx_val, exact)
				variance = np.square(variance)
				variance = [x / (len(variance)-1) for x in variance]
				print(sum(variance))
				avg = sum(error) / len(error)
				avg_errors.append(avg)
				d_bounds.append(avg / p)
				i+=1	
				p = helpers.getNextPrec(i)

			plt.figure(figure)
			plt1, = plt.plot(x_axis, avg_errors)
			plt.xlabel("Upper bound")
			plt.ylabel("Average error")
			plotLines1.append(plt1)
			plt.grid(True)

			plt.figure(figure+1)
			plt2, = plt.plot(x_axis, d_bounds)
			plt.xlabel("Upper bound")
			plt.ylabel("d_bound")
			plotLines2.append(plt2)
			plt.grid(True)

	str1 = "Wikidictionary (de)"
	str2 = "Wikiquote (en)"
	plt.figure(figure)
	plt.legend(plotLines1, [str1, str2])
	plt.figure(figure+1)
	plt.legend(plotLines2, [str1, str2])

	plt.show()



def plotTopKErrors():
	global figure

	with open(constants.epps_prec_results_json, 'r') as jfile:
		data = json.load(jfile)
	with open(constants.ground_truth_filename, 'r') as jfile:
		gt = json.load(jfile)

	
	for f in os.listdir(constants.root):
		exact_data = gt[f]
		exact = exact_data[constants.values_key]
		if helpers.fileFormat(f):
			apx_data = data[f]
			i = 0
			p = helpers.getNextPrec(i)

			plotLines1 = []
			plotLines2 = []
			while p <= constants.prec_to:
				apx = apx_data[str(p)]
				apx = apx[constants.values_key]
				error = sortedErrors(apx, exact)
				avg_error = []
				d_bounds = []
				pltLines = []
				for k in range(1, ks):
					cur_err = error[0:k]
					avg = sum(cur_err) / k
					avg_error.append(avg)
					d_bounds.append(avg / p)

				plt.figure(figure)
				plt1, = plt.plot(range(1, ks), avg_error)
				plt.ylabel('Average error')
				plt.xlabel('k') 
				plotLines1.append(plt1)
				plt.grid(True)

				plt.figure(figure+1)
				plt2, = plt.plot(range(1, ks), d_bounds)
				plt.legend()
				plt.ylabel('d_bound')
				plt.xlabel('k')
				plotLines2.append(plt2)
				plt.grid(True)

				i+=1
				p = helpers.getNextPrec(i)

			i = 0
			p = helpers.getNextPrec(i)
			str1 = "$\epsilon$ = "
			labels = []
			while p <= constants.prec_to:
				labels.append("%s%.2f" %(str1, p))
				i+=1
				p = helpers.getNextPrec(i)

			plt.figure(figure)
			plt.legend(plotLines1, labels)
			plt.figure(figure+1)
			plt.legend(plotLines2, labels)


		figure += 2

	plt.show()



def sortedErrors(apx, exa):
	approx = {str(i): apx[i] for i in range(0,len(apx))}
	exact = {str(i): exa[i] for i in range(0,len(exa))}
	sortedResult = sorted(exact.items(), key=itemgetter(1), reverse=True)
	return [abs(v[1] - approx[v[0]]) for v in sortedResult]


if __name__ == '__main__':
	main()