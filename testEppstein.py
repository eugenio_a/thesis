from graph_tool.all import *
import time
import sys
import os
from enum import Enum
import numpy as np
from epps import Eppstein
from epps import numberOfSamples
from operator import itemgetter
import json
import constants
import helpers
import csv

# Performance metrics
class Options(Enum):
		time = 'time'
		precision = 'precision'

def checkResultFile():
	try:
		with open(constants.epps_path, 'r') as f:
			pass
	except FileNotFoundError:
			with open(constants.epps_path, 'w') as f:
				json.dump(dict(), f)

def writeResult(f, p, time_elapsed, values, samples):
	checkResultFile()

	with open(constants.epps_path, 'r') as jfile:
		data = json.load(jfile)
		
		current_data_dict = dict.fromkeys([constants.time_key], time_elapsed)
		current_data_dict.update(dict.fromkeys([constants.samples_key], samples))
		current_data_dict.update(dict.fromkeys([constants.values_key], values))
		newDict = dict.fromkeys([p], current_data_dict)
		if f in data:
			newDict2 = data[f]
			newDict2.update(newDict)
			newDict2 = dict.fromkeys([f], newDict2)
		else:
			newDict2 = dict.fromkeys([f], newDict)

		data.update(newDict2)

	with open(constants.epps_path, 'w') as jfile:
		json.dump(data, jfile)

def main():

	# Checks if the ground truth file is present 
	# (no reason to continue if not)
	helpers.checkGroundTruth()

	### Default values, in case of no input arguments provided ###

	# Default precision value
	prec = 0.25
	# Time is the default option
	option = Options.time
	# By defalut the algorithm performs a single iteration
	iterations = 1	
	showPlots = False		

	# Read command line arguments
	args = sys.argv

	# This argument represents
	# the precision
	if (len(args) > 1):
		# Check if 0 <= prec <= 1
		prec = min(max(float(args[1]), 0), 1)
	else:
		print('No precision value provided!')
		sys.exit()

	# This argument represents the metric to be measured among
	# the networks provided in the folder: time or precision
	if (len(args) > 2):
		opt = str(args[2])
		if opt in constants.time_opt:
			option = Options.time
			helpers.printTimeHeader()
		elif opt in constants.prec_opt:
			option = Options.precision
			helpers.printPrecisionHeader()
			showPlots = True


	else:
		print('No time/precision metric provided!')
		sys.exit()

	# This argument represents the number of times the algorithm will
	# be executed on the test networks in order to calculate the average
	# of the time and precision performaces
	if (len(args) > 3):
		iterations = int(args[3])

	print('Eppstein will be executed %i times for each network' %(iterations))
	with open(constants.epps_prec_results_json, 'w') as jfile:
		json.dump({}, jfile)
	# Iterating for each file in
	# ./networks directory
	for f in os.listdir(constants.root):
		# Check the correct fileformat
		# (excludes system files)
		if helpers.fileFormat(f):# and helpers.checkEppsAlreadyDone(f):

			# Path to current network file
			path = constants.root + '/' + f
			print('Loading network %s ...' %(f))
			G = helpers.csvToGraph(path)
			print('Done!')

			print('Running Eppstein for graph %s ...' %(f))

			# Performing the precision test
			if option == Options.precision:
				i = 0


				while helpers.getNextPrec(i) <= constants.prec_to:
					print('Precision = %.2f' %(helpers.getNextPrec(i)))
					performPrecisionTest(G, helpers.getNextPrec(i), iterations, f)
					i += 1


				
			else:
				# Performing time test
				performTimeTest(G, prec, iterations, f)
		else:
			print('File %s not admitted or already done' %(f))


# Performs time test, no centrality
# values needed. Writes results on file
def performTimeTest(G, p, iterations, f):
	with open(constants.ground_truth_filename, 'r') as json_file:
	 	data = json.load(json_file)

	exact_result = data[str(f)]
	exact_seconds = exact_result[constants.time_key]

	total_time = 0
	if numberOfSamples(G.num_vertices(), p) < G.num_vertices():
		for i in range(0, iterations):
			print('Iteration #%i...' %(i + 1))

			start = time.perf_counter()
			Eppstein(G, p)
			time_elapsed = time.perf_counter() - start
			total_time += time_elapsed
			print('Finished in %s seconds' %(time_elapsed))

		total_time /= iterations
		metrics = dict.fromkeys(['network_name'] ,f)
		metrics.update(dict.fromkeys(["samples"], numberOfSamples(G.num_vertices(), p)))
		metrics.update(dict.fromkeys(['ratio'], helpers.getRatio(exact_seconds, total_time)))
	else:
		print('All vertices set needed, pass')

		metrics = dict.fromkeys(['network_name'] ,f)
		metrics.update(dict.fromkeys(["samples"], G.num_vertices()))
		metrics.update(dict.fromkeys(['ratio'], 0))


	result_string = helpers.timeResultString(metrics=metrics)
	print(result_string)
	result_file = open(constants.epps_time_results, 'a')
	result_file.write(result_string)
	result_file.close()

# Performs the precision test, compares the error
# and writes the result on file
def performPrecisionTest(G, p, iterations, f):

	total_time = 0
	approx_values = []
	if numberOfSamples(G.num_vertices(), p) < G.num_vertices():
		for i in range(0, iterations):
			print('Iteration #%i...' %(i + 1))
			start = time.perf_counter()
			approx_values.append(Eppstein(G, p))
			time_elapsed = time.perf_counter() - start
			total_time += time_elapsed

			print('Finished in %s seconds' %(time_elapsed))


			with open(constants.epps_prec_results_json, 'r') as jfile:
				data = json.load(jfile)


		approx_values = np.sum(approx_values, axis=0)
		approx_values = [v / iterations for v in approx_values]

		# newData = dict.fromkeys([constants.time_key, constants.values_key], 0.0)
		# newData[constants.time_key] = time_elapsed
		# newData[constants.values_key] = approx_values
		total_time /= iterations
		samples = numberOfSamples(G.num_vertices(), p)
		writeResult(f, round(p, 2), total_time, approx_values, samples)
		# data[f].update(dict.fromkeys([str(p)], newData))
		# with open(constants.epps_prec_results_json, 'w') as jfile:
		# 	json.dump(data, jfile)

	else:
		print('All vertices needed')




def sortedErrors(apx, exa):
	approx = {str(i): apx[i] for i in range(0,len(apx))}
	exact = {str(i): exa[i] for i in range(0,len(exa))}
	sortedResult = sorted(exact.items(), key=itemgetter(1), reverse=True)
	return [abs(v[1] - approx[v[0]]) for v in sortedResult]
	
if __name__ == '__main__':
	main()