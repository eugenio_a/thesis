Master thesis code

To perform test:

1) First create a ground truth: 
	python3 testGroundTruth.py dir k - for directed graphs
	python3 testGroundTruth.py undir k - for undirected graphs
where k is the number of times the algorithm will be executed, then the time performance is calculated averaging the results.

2) Test Eppstein algorithm:
	python3 testEppstein.py <precision: (0,1) values> <'time' | 'prec'> <'dir' | 'undir'>

3) Test Okamoto algorithm:
	python3 testOkamoto.py <k (value of k for top-k)> <'time' | 'ks'> <'dir' | 'undir'>
