import networkx as nx
from graph_tool.all import *
from operator import itemgetter

# Calculates exact harmonic centrality for the set of vertices S
def harmonic(G, S):
	return {v: closeness(G, source=G.vertex(v), harmonic=True, norm=True) for v in S}

def topk_harmonic(G, k):
	if k > 0:
		all_harmonics = sorted(nx.algorithms.centrality.harmonic.harmonic_centrality(G).items(), key=itemgetter(1), reverse=True)
		start = k-1
		n = len(G.nodes())
		while start < n and all_harmonics[start+1][1] == all_harmonics[start][1]:
			start+=1
		result = dict((all_harmonics[0:min(start + 1, n)]))
		result.update((k, v/(n-1)) for k,v in result.items())
		return result
	return

if __name__ == '__main__':
	main()

def main():
	pass