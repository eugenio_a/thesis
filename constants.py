# Location of the networks
root = "./networks"

# Keys for result dictionaries
time_key = 'time'
values_key = 'values'
samples_key = 'samples'
add_samples_key = 'add_samples'
precision_key = 'precision'

# Ground truth file names
ground_truth_filename = 'ground_truth.json'
epps_prec_results_json = 'eppstein_prec_result.json'
epps_time_results = 'Eppstein_time_results.csv'
epps_prec_results = 'Eppstein_prec_results.csv'
oka_results = 'Okamoto_results.csv'
borassi_path = './results/borassi/borassi_results'
epps_path = './results/eppstein/epps_results'
oka_path = './results/okamoto/oka_results'
oka_path_first = './results/okamoto/oka_results'

# Location of the networks
root = "./networks"

# Possible inputs as algorithm arguments
time_opt = ['t', 'T', 'time', 'Time']
prec_opt = ['p', 'P', 'prec', 'Prec', 'precision', 'Precision']
directed = ['dir', 'directed']

### Precisions list for Eppstein ###
samplesConstants = 1
# Okamoto constant
oka_const = 1.01
# Okamoto constants for samples
oka_samples_const = 1
# Lowest precision value
prec_from = 0.05
# Highest precision value
prec_to = 0.5
# Precision granularity
prec_gran = 0.05

### Ks list for Okamoto ###
k_list = [1, 10, 100]

# Error granularity
errorGranularity = 0.001