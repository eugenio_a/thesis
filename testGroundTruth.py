from graph_tool.all import *
import sys
import os
import time
from helpers import fileFormat
import constants
import json
import helpers

# Tests the performance of the naive algorithm with the
# networks provided in the folder "networks"

def checkGroundTruth():
	try:
		with open(constants.ground_truth_filename, 'r') as f:
			pass
	except FileNotFoundError:
			with open(constants.ground_truth_filename, 'w') as f:
				json.dump(dict(), f)

# Writes the ground truth on a .json file
def writeToFile(f, result):
	checkGroundTruth()

	with open(constants.ground_truth_filename, 'r') as jfile:
		data = json.load(jfile)

	data.update(dict.fromkeys([f], result))

	with open(constants.ground_truth_filename, 'w') as jfile:
		json.dump(data, jfile)


def main():

	# Read command line arguments
	args = sys.argv

	# Check if the first argument
	# represents directed graphs
	# if (len(args) > 1):
	# 	dirArg = str(args[1])
	# 	if dirArg in constants.directed:
	# 		directed = True

	# Number of times the algorithm will be
	# executed in order to get a better
	# time performance estimation
	iterations = 1
	if (len(args) > 1):
		iterations = max(iterations, int(args[1]))

	result_to_write = {}

	# Loop for each file in the
	# directory '/networks'
	for f in os.listdir(constants.root):

		if fileFormat(f) and helpers.checkGroundTruthAlreadyDone(f):

			# Path to current network file
			path = constants.root + '/' + f

			# Reads and creates a new graph object
			G = helpers.csvToGraph(path)

			print('Running exact algorithm for graph %s ...' %(f))

			total_time = 0

			for i in range(0, iterations):

				print('Iteration #%i' %(i + 1))

				# Timestamp before the algorithm
				# starts
				start = time.perf_counter()

				# Executing the algorithm
				result = closeness(G, harmonic=True, norm=True)

				time_elapsed = time.perf_counter() - start
				total_time += time_elapsed
				print('Finished in %s seconds' %(time_elapsed))

			avg_time = total_time / iterations
			serializable_result = [v for v in result]
			
			print('Writing data to disk...')
			
			result_to_write[str(f)] = {constants.time_key: avg_time, constants.values_key: serializable_result}
			writeToFile(f, result_to_write)


	print('All done!')
	
if __name__ == '__main__':
	main()