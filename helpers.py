import constants
from graph_tool.all import *
import csv
import numpy as np
import json

# Calculates the improvement ratio
def getRatio(tn, ta):
	return (tn - ta) / tn

def revRatio(ratio, tn):
	return -(ratio * tn - tn)

# Checks if the ground truth has been computed
def checkGroundTruth():
	try:
		with open(constants.ground_truth_filename, 'r') as f:
			pass
	except FileNotFoundError:
		print('Error: no ground truth file found, execute \'python3 test.py exact\' first!')
		sys.exit()

def totalError(exact, apx):
	return sum(np.absolute(np.subtract(exact, apx)))

def variance(exact, apx):
	return sum(np.square(np.subtract(exact, apx))) / (len(exact) - 1)

def relError(exact, apx):
	#difference = np.sum([exact, np.absolute(np.subtract(exact, apx))], axis=0)
	difference = np.absolute(np.subtract(exact, apx))
	return sum([difference[i] / exact[i] if exact[i] > 0 else difference[i] for i in range(0,len(exact))]) / len(exact)

def maxError(exact, apx):
	return max(np.absolute(np.subtract(exact, apx)))

# Checks if the file format is csv
def fileFormat(file):
	return file.endswith("csv")

def checkEppsAlreadyDone(file):
	with open(constants.epps_path, 'r') as f:
		data = json.load(f)

	return not file in data

def checkGroundTruthAlreadyDone(file):
	with open(constants.ground_truth_filename, 'r') as f:
		data = json.load(f)

	return not file in data

def checkNetK(network, k):
	try:
		with open(constants.oka_path, 'r') as jfile:
			data = json.load(jfile)

		if str(network) in data:
			net_data = data[str(network)]
			if str(k) in net_data:
				return False
		return True

	except FileNotFoundError:
		return True

# Checks if the topk values found by Okamoto
# are correct
def checkTopK(exact, approx):
	exact_topk = sorted(exact, reverse=True)
	exact_topk = exact_topk[0: len(approx)]
	exact_set = set(exact_topk)
	apx_set = set(approx)
	intersection_size = len(exact_set.intersection(apx_set))
	return [100 * intersection_size / len(exact_set), len(exact_set) - intersection_size]

# Reads graph from csv file
def csvToGraph(path):
	return load_graph_from_csv(path, ecols=(0, 1), directed=False, csv_options={'delimiter': ' '})

# Prints the first line of the precision performances in both command line and file
def printPrecisionHeader():
	with open(constants.epps_prec_results, 'w', newline='') as csvfile:
		csvfile.write('\n%s' %(precisionResultTitle()))
	print(precisionResultTitle())

# Prints the first line of the time performances in both command line and file
def printTimeHeader():
	with open(constants.epps_time_results, 'w', newline='') as csvfile:
		csvfile.write(timeResultTitle())
	print(timeResultTitle())

# Returns the first line in the precision performances file
def precisionResultTitle():
	return '{:^10}  {:^14}  {:^7}  {:^6}  {:^11}  {:^8} {:^8} {:^12} {:^8}\n\n'.format('PRECISION', 'TIME', 'SAMPLES', 'RATIO', 'ERROR', 'AVG_ERROR', 'MAX_ERR', 'D_BOUND', 'VAR')

# Returns the first line in the time performances file
def timeResultTitle():
	return '{:^30}  {:^30}  {:^30}\n\n'.format('NETWORK', 'SAMPLES', 'RATIO')

# Prints a single row in the precision performances file
def precResultString(metrics):
		return '{:^04.2f}  {:^14.9f}  {:^7}  {:^04.2f}  {:^11.6f}  {:^8.10f} {:^8.10f}  {:^8.6f}  {:^8.6f}\n'.format(
		metrics["prec"], 
		metrics["time"], 
		metrics["samples"], 
		metrics["ratio"]*100, 
		metrics["error"], 
		metrics["avg_error"],
		metrics["max_error"], 
		metrics["d_bound"], 
		metrics["var"])

def printTopKError(error, p):
	ks = [1, 10, 20, 50, 100]
	for k in ks:
		cur_err = error[0:k]
		avg = sum([x / k for x in cur_err])
		var = sum([pow(x - avg, 2) for x in cur_err]) / max(1, k-1)
		print("K = %i, max_error = %f, avgError = %f, d_bound = %f, var = %f" %(k, max(cur_err), avg, avg/p, var))


# Prints a single row in the time performances file
def timeResultString(metrics):
	return '{:^30}  {:^30}  {:^30}\n'.format(
		metrics['network_name'], 
		metrics['samples'], 
		metrics['ratio'])

# Returns the next precision value, used in Eppstein tests
def getNextPrec(index):
# 	if index == 0:
# 		return constants.prec_from
# 	if index == 1:
# 		return constants.prec_from + 0.04
	return round(constants.prec_from + constants.prec_gran * index, 2)

def main():
	pass

if __name__ == '__main__':
	main()